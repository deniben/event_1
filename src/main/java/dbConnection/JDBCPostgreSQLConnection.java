package dbConnection;

//STEP 1. Import required packages
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;


public class JDBCPostgreSQLConnection {

    public static Connection getPostgresSQLConnection()
            throws ClassNotFoundException, SQLException {
        //  Database credentials
        String hostName = "localhost";
        String dbName = "event"; //
        String user = "postgres";
        String pass = "root"; //postgres or root
        return getPostgresSQLConnection(hostName, dbName, user, pass);
    }

    public static Connection getPostgresSQLConnection(String hostName, String dbName,
                                                      String userName, String password) throws SQLException,
            ClassNotFoundException {

        Class.forName("org.postgresql.Driver");

        // Структура URL Connection для MySQL:
        String connectionURL = "jdbc:postgresql://" + hostName + ":5433/" + dbName; //5432 or 5433

        Connection conn = DriverManager.getConnection(connectionURL, userName, password);
        return conn;
    }
}
